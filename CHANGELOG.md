## [0.3.2] - 22.01.2021

* Updated dependencies.

## [0.0.2] - 01.09.2020 

* Adjusted environment.

## [0.0.1] - 01.09.2020 

* First release
